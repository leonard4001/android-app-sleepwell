package com.palme.sleepwell.persistance

import android.content.Context
import androidx.annotation.StringRes
import com.palme.sleepwell.R

/**
 *
 *  This class represents the persistence layer of the app.
 *  It provides an easy to use interface for activities that need to store information in the shared preferences
 *
 * @author Leonard Palm
 */
class Prefs(private val context: Context) {

    private val preferences = context.getSharedPreferences(PREF_FILE_NAME, 0)

    var alarmTimestamp: Long
        get() = preferences.getLong(key(R.string.pref_key_alarm_timestamp), 0L)
        set(value) = preferences.edit().putLong(key(R.string.pref_key_alarm_timestamp), value).apply()

    var chillTime: Int
        get() = preferences.getInt(key(R.string.pref_key_chill_time), 0)
        set(value) = preferences.edit().putInt(key(R.string.pref_key_chill_time), value).apply()

    private fun key(@StringRes keyRes: Int): String = context.getString(keyRes)

    companion object {

        private const val PREF_FILE_NAME = "com.palme.sleepwell.preferences"
    }
}