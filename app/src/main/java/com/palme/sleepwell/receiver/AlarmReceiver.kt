package com.palme.sleepwell.receiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.palme.sleepwell.R
import com.palme.sleepwell.activity.AlarmActivity
import com.palme.sleepwell.persistance.Prefs
import com.palme.sleepwell.util.getInt

/**
 *
 *  This is the BroadcastReceiver that gets started if the pending intent for the planned alarm goes off.
 *  It double-checks if the current planned alarm time (from the shared preferences) matches the intended
 *  time this intent was planned for.
 *  This is necessary because the AlarmManager has some delay with cancelling pending intents
 *
 * @author Leonard Palm
 */
class AlarmReceiver: BroadcastReceiver() {

    /**
     *
     * @param context The application context
     * @param intent The intent that was created within the pending intent
     */
    override fun onReceive(context: Context?, intent: Intent?) {

        val preferences = Prefs(context!!)

        // Check if the active alarm was already cancelled, but this pending intent fired anyways
        if( preferences.alarmTimestamp == 0L ){
            return
        }

        // Get the provided exact alarm time the pending intent was planned for
        val alarmTimeIntent = intent!!.getLongExtra(EXTRA_ALARM_TIME, 0L)

        // Check if this alarm fits the current active alarm time
        if( preferences.alarmTimestamp != alarmTimeIntent ){
            return
        }

        // Cancel the upcoming alarm notification
        val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(context.getInt(R.integer.notification_id_upcoming_alarm))


        // Fire off the alarm
        val alarmIntent = Intent(context, AlarmActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        context.startActivity(alarmIntent)
    }

    companion object {

        /**
         * Used by the ConfirmAlarmActivity to fill the intent with the planned alarm time
         * (Used for the double-check)
         */
        const val EXTRA_ALARM_TIME = "EXTRA_ALARM_TIME"
    }
}