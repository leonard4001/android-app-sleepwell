package com.palme.sleepwell.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import org.joda.time.LocalTime

/**
 *
 *  The ViewModel class for the AlarmActivity
 *
 * @author Leonard Palm
 */
class ViewModelAlarmActivity(application: Application): AndroidViewModel(application) {

    val liveTime: MutableLiveData<LocalTime> = MutableLiveData()
}