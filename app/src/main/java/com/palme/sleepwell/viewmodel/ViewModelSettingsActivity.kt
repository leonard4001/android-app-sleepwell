package com.palme.sleepwell.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.palme.sleepwell.persistance.Prefs

class ViewModelSettingsActivity(application: Application): AndroidViewModel(application) {

    private val preferences = Prefs(application)

    var chillTime: Int
        get() = preferences.chillTime
        set(it) {
            preferences.chillTime = it
        }
}