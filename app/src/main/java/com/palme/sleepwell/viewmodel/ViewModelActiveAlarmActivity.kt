package com.palme.sleepwell.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

/**
 *
 *  The ViewModel class for the ActiveAlarmActivity
 *
 * @author Leonard Palm
 */
class ViewModelActiveAlarmActivity(application: Application): AndroidViewModel(application) {

    val activeAlarmTimestamp: MutableLiveData<Long> = MutableLiveData()
}