package com.palme.sleepwell.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.palme.sleepwell.persistance.Prefs
import org.joda.time.LocalTime

/**
 *
 *  The ViewModel class for the ConfirmAlarmActivity
 *
 * @author Leonard Palm
 */
class ViewModelConfirmAlarmActivity(application: Application): AndroidViewModel(application) {

    val sleepAmount: MutableLiveData<Int> = MutableLiveData()

    val liveTime: MutableLiveData<LocalTime> = MutableLiveData()

    private val preferences = Prefs(application)

    val chillTime: Int
        get() = preferences.chillTime

    var alarmTimestamp: Long
        get() = preferences.alarmTimestamp
        set(value) {
            preferences.alarmTimestamp = value
        }

}