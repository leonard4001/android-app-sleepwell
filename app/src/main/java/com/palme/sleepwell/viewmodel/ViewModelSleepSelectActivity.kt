package com.palme.sleepwell.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.palme.sleepwell.persistance.Prefs
import org.joda.time.LocalTime

/**
 *
 *  The ViewModel class for the SleepSelectActivity
 *
 * @author Leonard Palm
 */
class ViewModelSleepSelectActivity(application: Application) : AndroidViewModel(application) {

    val liveTime: MutableLiveData<LocalTime> = MutableLiveData()

    private val preferences = Prefs(application)

    var alarmTimestamp: Long
        get() = preferences.alarmTimestamp
        set(value){
            preferences.alarmTimestamp = value
        }

    var chillTime: Int
        get() = preferences.chillTime
        set(value) {
           preferences.chillTime = value
        }

    init {


    }
}