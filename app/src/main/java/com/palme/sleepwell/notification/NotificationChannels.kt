package com.palme.sleepwell.notification

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.content.ContextCompat
import com.palme.sleepwell.R

/**
 *
 * Used for the notification channel creation
 * There is only one for this app named 'Default Channel'
 *
 * @author Leonard Palm
 */

internal const val NOTIFICATION_CHANNEL_DEFAULT = "NOTIFICATION_CHANNEL_DEFAULT"

@TargetApi(Build.VERSION_CODES.O)
fun createNotificationChannel(context: Context): NotificationChannel{

    // Create channel for plan updates
    val channelDefault = NotificationChannel(NOTIFICATION_CHANNEL_DEFAULT,
        context.getString(R.string.notification_channel_default),
        NotificationManager.IMPORTANCE_LOW).apply {

        description = context.getString(R.string.notification_channel_default_description)
        enableLights(true)
        lightColor = ContextCompat.getColor(context, R.color.primary_color)
        enableVibration(true)
    }

    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        .createNotificationChannel(channelDefault)

    return channelDefault
}