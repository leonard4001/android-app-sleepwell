package com.palme.sleepwell.recyclerview.viewholder

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.palme.sleepwell.R
import com.palme.sleepwell.activity.SleepSelectActivity
import com.palme.sleepwell.util.print

/**
 *
 *  The viewholder for the select sleep hours recyclerview-list
 *  It observes the live clock from the SleepSelectActivity, to update its wake up times
 *
 * @author Leonard Palm
 */
class ViewHolderSleepHours(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val cvSleepHours: CardView = itemView.findViewById(R.id.cvSleepHours)
    private val tvSleepHours: TextView = itemView.findViewById(R.id.tvSleepHours)
    private var tvSleepHoursDaytime: TextView = itemView.findViewById(R.id.tvSleepHoursDaytime)

    /**
     *
     * Binding the activity instance and the sleep hours amount to this specific card
     *
     * @param sleepSelectActivity The instance of the SleepSelectActivity hosting this recyclerview
     * @param hours The sleep hours of this card
     */
    fun bind(sleepSelectActivity: SleepSelectActivity, hours: Int){

        tvSleepHours.text = hours.toString()

        // Subscribe to the live timer of the hosting SleepSelectActivity activity
        sleepSelectActivity.viewModel.liveTime.observe(sleepSelectActivity, Observer {

            // Update the wake-up time view with the live time
            tvSleepHoursDaytime.text = it.plusHours(hours).print()
        })
    }
}