package com.palme.sleepwell.recyclerview.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.palme.sleepwell.R
import com.palme.sleepwell.activity.ConfirmAlarmActivity
import com.palme.sleepwell.activity.SleepSelectActivity
import com.palme.sleepwell.recyclerview.viewholder.ViewHolderSleepHours

/**
 *
 *  The adapter for the select sleep hours recyclerview-list
 *
 * @author Leonard Palm
 */
class AdapterSleepHours(private val sleepSelectActivity: SleepSelectActivity): RecyclerView.Adapter<ViewHolderSleepHours>(){

    /**
     * A List containing all intended sleep hours
     */
    private var hours: List<Int> = mutableListOf()

    /**
     *
     * Updates the list with the new set of sleep hours
     *
     * @param hours A List containing all intended sleep hours, that should be shown in this list
     */
    fun updateList(hours: List<Int>){

        this.hours = hours
        notifyDataSetChanged()
    }

    /**
     *
     * Creating the ViewHolderSleepHours instance by inflating the item view
     *
     * @param parent The parent ViewGroup object
     * @param viewType The view type of the card (there is only one for this recyclerview)
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderSleepHours {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.card_sleep_hour, parent, false)

        return ViewHolderSleepHours(itemView)
    }

    /**
     *
     * The viewholder instance gets it's data (the sleep amount in hours)
     *
     * @param holder An instance of class ViewHolderSleepHours
     * @param position The card id (starting at zero)
     */
    override fun onBindViewHolder(holder: ViewHolderSleepHours, position: Int) {

        val sleepAmount = hours.getOrNull(position)

        if( sleepAmount != null ) {
            holder.bind(sleepSelectActivity, sleepAmount)
        }

        holder.cvSleepHours.setOnClickListener { onCardSelected(position) }
    }

    /**
     * Returns the amount of card (or sleep hours that are shown)
     */
    override fun getItemCount(): Int = hours.size

    /**
     *
     * Called when a card was clicked
     *
     * @param position The card-id that was clicked (starting at zero)
     */
    private fun onCardSelected(position: Int){

        //Create intent containing sleep amount
        val intent = Intent(sleepSelectActivity, ConfirmAlarmActivity::class.java).apply {
            putExtra(ConfirmAlarmActivity.EXTRA_SLEEP_AMOUNT, hours[position])
        }

        sleepSelectActivity.startActivity(intent)
    }
}