package com.palme.sleepwell.recyclerview.decorator

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 *
 * The card-spacing decorator used by the sleep hours recyclerview-list
 *
 * @author Leonard Palm
 */
class DecoratorListSleepHours: RecyclerView.ItemDecoration() {

    /**
     *
     * Used to provide the item view with the left, right, top and bottom spacing
     *
     * @param outRect The rectangle object
     * @param view The item view
     * @param parent The recyclerview
     * @param state The state of the recyclerview
     */
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        outRect.top = SPACING_TOP_BOTTOM
        outRect.bottom = SPACING_TOP_BOTTOM
        outRect.left = SPACING_LEFT_RIGHT
        outRect.right = SPACING_LEFT_RIGHT
    }

    companion object {

        private const val SPACING_TOP_BOTTOM = 24
        private const val SPACING_LEFT_RIGHT = 16
    }
}