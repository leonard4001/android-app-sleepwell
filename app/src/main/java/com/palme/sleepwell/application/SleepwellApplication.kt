package com.palme.sleepwell.application

import android.app.Application
import net.danlew.android.joda.JodaTimeAndroid

/**
 *
 * The main application class of the app.
 * Created for the Joda-Time library to initialize itself
 *
 * @author Leonard Palm
 */
@Suppress("unused")
class SleepwellApplication: Application() {

    override fun onCreate() {

        super.onCreate()
        JodaTimeAndroid.init(this)
    }
}