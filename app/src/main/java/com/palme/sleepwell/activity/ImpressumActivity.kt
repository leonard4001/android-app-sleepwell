package com.palme.sleepwell.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.palme.sleepwell.R

/**
 *
 * Activity to show me
 *
 * @author Leonard Palm
 */
class ImpressumActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_impressum)
    }
}
