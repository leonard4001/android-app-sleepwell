package com.palme.sleepwell.activity

import android.app.KeyguardManager
import android.media.AudioAttributes
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.palme.sleepwell.R
import com.palme.sleepwell.persistance.Prefs
import com.palme.sleepwell.util.currentTime
import com.palme.sleepwell.util.getViewModelInstance
import com.palme.sleepwell.util.print
import com.palme.sleepwell.viewmodel.ViewModelAlarmActivity
import kotlinx.android.synthetic.main.activity_alarm.*
import kotlinx.coroutines.*
import org.joda.time.LocalTime
import kotlin.coroutines.CoroutineContext

/**
 *
 * The activity that's shown, when the alarm goes off
 *
 * @author Leonard Palm
 */
class AlarmActivity : AppCompatActivity(), CoroutineScope {

    private var mJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    private lateinit var mediaPlayer: MediaPlayer

    private lateinit var viewModel: ViewModelAlarmActivity

    private val preferences by lazy {
        Prefs(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_alarm)

        // Keep screen always on, unless the user interacts (wakes the fuck up...)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        // Turn on the screen and display this activity over the lockscreen of the device
        setTurnScreenOn(true)
        setShowWhenLocked(true)
        (getSystemService(KeyguardManager::class.java) as KeyguardManager).requestDismissKeyguard(this, null)

        viewModel = getViewModelInstance()

        bnAlarmTurnOff.setOnClickListener { turnOffAlarm() }

        viewModel.liveTime.observe(this, liveTimeObserver)

        startTimer()

        // Start the alarm ring tone
        ring()
    }

    private fun startTimer(){

        mJob = launch {

            while (true){
                viewModel.liveTime.postValue(currentTime())
                delay(1000)
            }
        }
    }

    private val liveTimeObserver = Observer<LocalTime>{
        tvAlarmDaytime.text = it.print()
    }

    private fun ring(){

        val alarmToneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM)

        mediaPlayer = MediaPlayer().apply {
            setDataSource(this@AlarmActivity, alarmToneUri)
            setAudioAttributes(
                AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM).build()
            )
            isLooping = true
        }

        mediaPlayer.prepare()
        mediaPlayer.start()

        ivAlarm.startAnimation(
            AnimationUtils.loadAnimation(this, R.anim.alarm_shake)
        )
    }

    private fun turnOffAlarm(){

        mediaPlayer.stop()

        // Delete the alarm time from the shared preferences
        preferences.alarmTimestamp = 0L

        // Leave the activity
        finish()
    }

    override fun onBackPressed() {
        // Don't do anything when the user presses 'back'
    }

    override fun onDestroy() {

        super.onDestroy()

        // Release the player
        mediaPlayer.reset()
        mediaPlayer.release()

        mJob.cancel()
    }
}
