package com.palme.sleepwell.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.palme.sleepwell.R
import kotlinx.android.synthetic.main.activity_license_read.*

/**
 *
 * Provides the license of the library in an webview
 *
 * @author Leonard Palm
 */
class LicenseReadActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_license_read)
    }

    override fun onStart() {

        super.onStart()

        val url = intent.getStringExtra(EXTRA_LICENSE_URL)

        if( url != null ) {
            wvLicense.loadUrl(url)
        }
    }

    companion object {

        const val EXTRA_LICENSE_URL = "EXTRA_LICENSE_URL"
    }
}
