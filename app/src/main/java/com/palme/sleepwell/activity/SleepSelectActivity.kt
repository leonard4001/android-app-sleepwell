package com.palme.sleepwell.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.palme.sleepwell.R
import com.palme.sleepwell.recyclerview.adapter.AdapterSleepHours
import com.palme.sleepwell.recyclerview.decorator.DecoratorListSleepHours
import com.palme.sleepwell.util.currentTime
import com.palme.sleepwell.util.getIntArray
import com.palme.sleepwell.util.getViewModelInstance
import com.palme.sleepwell.util.startActivitySimple
import com.palme.sleepwell.viewmodel.ViewModelSleepSelectActivity
import kotlinx.android.synthetic.main.activity_sleep_select.*
import kotlinx.coroutines.*
import org.joda.time.LocalDateTime
import java.util.*
import kotlin.coroutines.CoroutineContext

/**
 *
 * This activity lists the sleep hours times.
 * If the user selects one, the ConfirmAlarmActivity gets started
 *
 * @author Leonard Palm
 */
class SleepSelectActivity : AppCompatActivity(), CoroutineScope {

    private var mJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    private lateinit var adapterSleepHours: AdapterSleepHours
    lateinit var viewModel: ViewModelSleepSelectActivity

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_select)

        viewModel = getViewModelInstance()

        // Check if chill-time is set in preferences
        if( viewModel.chillTime == 0 ){
            // Initialize the chill time to 5 minutes
            viewModel.chillTime = 5
        }

        // Check if there is an alarm set
        if( viewModel.alarmTimestamp != 0L ){

            if( isOldAlarm(viewModel.alarmTimestamp) ){
                // If there is still an old alarm saved, delete it
                viewModel.alarmTimestamp = 0L
            }else {
                // This alarm is in the future, so show its stats and leave this activity
                leaveToActiveAlarmActivity(viewModel.alarmTimestamp)
                return
            }
        }

        displaySleepHours()
        startLiveTimer()

        // Start SettingsActivity on click
        ivSettings.setOnClickListener {
            startActivitySimple(SettingsActivity::class.java)
        }
    }

    private fun leaveToActiveAlarmActivity(alarmTimestamp: Long){

        val activeAlarmIntent = Intent(this, ActiveAlarmActivity::class.java).apply {
            putExtra(ActiveAlarmActivity.EXTRA_ALARM_TIMESTAMP, alarmTimestamp)
        }

        startActivity(activeAlarmIntent)
        finish()
    }

    private fun displaySleepHours(){

        adapterSleepHours = AdapterSleepHours(this)

        rvSleepHours.adapter = adapterSleepHours
        rvSleepHours.layoutManager = LinearLayoutManager(this)
        rvSleepHours.itemAnimator = DefaultItemAnimator()
        rvSleepHours.addItemDecoration(DecoratorListSleepHours())

        adapterSleepHours.updateList(getIntArray(R.array.sleep_hours).asList())
    }

    private fun startLiveTimer(){

        mJob = launch {

            while(true){
                viewModel.liveTime.postValue(currentTime())
                delay(1000)
            }
        }
    }

    private fun isOldAlarm(alarmTimestamp: Long): Boolean =
        LocalDateTime.fromDateFields(Date(alarmTimestamp)).isBefore(LocalDateTime.now())

    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }
}
