package com.palme.sleepwell.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_licenses.*
import android.widget.ArrayAdapter
import com.palme.sleepwell.R
import com.palme.sleepwell.activity.LicenseReadActivity.Companion.EXTRA_LICENSE_URL
import com.palme.sleepwell.util.getStringArray

/**
 *
 * This activity lists all used libraries
 *
 * @author Leonard Palm
 */
class LicensesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_licenses)

        displayList()
    }

    private fun displayList(){

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, getStringArray(R.array.licenses))

        lvLicenses.adapter = adapter

        lvLicenses.setOnItemClickListener { _, _, position, _ ->

            val url = when( position ){
                0, 1, 2, 3, 4, 5, 6, 7, 8 -> APACHE_V2_URL
                else -> null
            }

            if( url != null ) {
                startActivity(
                    Intent(this@LicensesActivity, LicenseReadActivity::class.java).apply {
                        putExtra(EXTRA_LICENSE_URL, url)
                    }
                )
            }
        }
    }

    companion object {

        private const val APACHE_V2_URL = "http://www.apache.org/licenses/LICENSE-2.0.txt"
    }
}
