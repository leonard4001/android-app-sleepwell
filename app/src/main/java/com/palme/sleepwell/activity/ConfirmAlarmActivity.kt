package com.palme.sleepwell.activity

import android.app.AlarmManager
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.palme.sleepwell.receiver.AlarmReceiver
import com.palme.sleepwell.R
import com.palme.sleepwell.notification.createNotificationChannel
import com.palme.sleepwell.util.*
import com.palme.sleepwell.viewmodel.ViewModelConfirmAlarmActivity
import kotlinx.android.synthetic.main.activity_confirm_alarm.*
import kotlinx.coroutines.*
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import java.util.*
import kotlin.coroutines.CoroutineContext

/**
 *
 * This activity is used to show the sleeping timeline with the exact sleep and wake up times.
 * The user confirm the alarm with the main button
 *
 * @author Leonard Palm
 */
class ConfirmAlarmActivity : AppCompatActivity(), CoroutineScope{

    private var mJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = mJob + Dispatchers.Main

    private lateinit var viewModel: ViewModelConfirmAlarmActivity

    private val alarmManager by lazy {
        getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    private val notificationManager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_alarm)

        viewModel = getViewModelInstance()

        startLiveTimer()
        viewModel.liveTime.observe(this, liveTimeObserver)

        viewModel.sleepAmount.observe(this, sleepHourObserver)

        viewModel.sleepAmount.postValue(getExtraInt(EXTRA_SLEEP_AMOUNT))

        bnAlarmActivate.setOnClickListener { scheduleAlarm() }

        animateViews()
    }

    private fun animateViews() {

        // Animate the bottom button
        val animButtonSlideIn = AnimationUtils.loadAnimation(this, R.anim.button_push_in)

        animButtonSlideIn.addEndListener { bnAlarmActivate.visibility = View.VISIBLE }
        animButtonSlideIn.startOffset = 1000
        bnAlarmActivate.startAnimation(animButtonSlideIn)

        // Animate all 3 dots of the timeline
        val animScaleInDotNow = AnimationUtils.loadAnimation(this, R.anim.scale_in)
        val animScaleInDotSleep = AnimationUtils.loadAnimation(this, R.anim.scale_in)
        val animScaleInDotAlarm = AnimationUtils.loadAnimation(this, R.anim.scale_in)

        animScaleInDotNow.startOffset = 100
        animScaleInDotSleep.startOffset = 400
        animScaleInDotAlarm.startOffset = 700

        vAlarmTimepointSleep.startAnimation(animScaleInDotSleep)
        vAlarmTimepointAlarm.startAnimation(animScaleInDotAlarm)
        vAlarmTimepointNow.startAnimation(animScaleInDotNow)

        //Animate 2 timeline lines
        val animGrowTimeline1 = AnimationUtils.loadAnimation(this, R.anim.timeline_grow)
        val animGrowTimeline2 = AnimationUtils.loadAnimation(this, R.anim.timeline_grow)

        animGrowTimeline1.startOffset = 400
        animGrowTimeline1.duration = 200

        vAlarmTimelineChill.startAnimation(animGrowTimeline1)

        animGrowTimeline2.startOffset = 600
        animGrowTimeline2.duration = 400

        vAlarmTimelineSleep.startAnimation(animGrowTimeline2)

        // Animate times on the left
        val animSlideInToLeftNow = AnimationUtils.loadAnimation(this, R.anim.slide_in_to_left)
        val animSlideInToLeftSleep= AnimationUtils.loadAnimation(this, R.anim.slide_in_to_left)
        val animSlideInToLeftAlarm = AnimationUtils.loadAnimation(this, R.anim.slide_in_to_left)

        tvAlarmTimeNow.startAnimation(animSlideInToLeftNow)
        tvAlarmTimeSleep.startAnimation(animSlideInToLeftSleep)
        tvAlarmTimeAlarm.startAnimation(animSlideInToLeftAlarm)

        // Animate timeline descriptions
        val animSlideInToRightChill = AnimationUtils.loadAnimation(this, R.anim.slide_in_to_right)
        val animSlideInToRightSleep = AnimationUtils.loadAnimation(this, R.anim.slide_in_to_right)
        val animSlideInToRightGetUp = AnimationUtils.loadAnimation(this, R.anim.slide_in_to_right)

        tvAlarmTimelineChill.startAnimation(animSlideInToRightChill)
        tvAlarmTimelineSleepAmount.startAnimation(animSlideInToRightSleep)
        tvAlarmTimepointGetUp.startAnimation(animSlideInToRightGetUp)
    }

    private val sleepHourObserver = Observer<Int> {

        when(it){
            -1 -> {
                finish()
            }
            else -> {
                tvAlarmTimelineChill.text = getString(R.string.chill, viewModel.chillTime)
                tvAlarmTimelineSleepAmount.text = getString(R.string.timeline_sleep, it)
            }
        }
    }

    private val liveTimeObserver = Observer<LocalTime> {

        tvAlarmTimeSleep.text = it.plusMinutes(viewModel.chillTime).print()
        tvAlarmTimeAlarm.text = it.plusMinutes(viewModel.chillTime).plusHours(viewModel.sleepAmount.value!!).print()
    }

    private fun startLiveTimer(){

        mJob = launch {

            while(true){
                viewModel.liveTime.postValue(currentTime())
                delay(1000)
            }
        }
    }

    private fun scheduleAlarm(){

        // Set exact alarm timestamp
        val exactAlarmTimestamp = getExactDateTimeForAlarm(viewModel.sleepAmount.value!!, viewModel.chillTime).timeInMillis()

        // Create pending intent to fire when alarm goes off
        val pendingAlarmIntent = PendingIntent.getBroadcast(
            this,
            exactAlarmTimestamp.toString().hashCode(),
            Intent(this, AlarmReceiver::class.java).apply {
                putExtra(AlarmReceiver.EXTRA_ALARM_TIME, exactAlarmTimestamp)
            },
            PendingIntent.FLAG_CANCEL_CURRENT)

        // Schedule the alarm
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, exactAlarmTimestamp, pendingAlarmIntent)

        // Save active alarm to preferences
        viewModel.alarmTimestamp = exactAlarmTimestamp

        createAlarmNotification(exactAlarmTimestamp)

        // Leave activity
        leaveToActiveAlarmActivity(exactAlarmTimestamp)
    }

    private fun createAlarmNotification(exactAlarmTimestamp: Long) {

        val alarmDateTime = LocalDateTime.fromDateFields(Date(exactAlarmTimestamp))

        @Suppress("DEPRECATION")
        val notificationBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder(this, createNotificationChannel(this).id)
        } else {
            Notification.Builder(this)
        }

        // Build PendingIntent that's executed on notification click
        val clickIntent = Intent(this, ActiveAlarmActivity::class.java).apply {

            flags =
                Intent.FLAG_ACTIVITY_NEW_TASK or
                Intent.FLAG_ACTIVITY_CLEAR_TASK

            putExtra(ActiveAlarmActivity.EXTRA_ALARM_TIMESTAMP, exactAlarmTimestamp)
        }

        val clickPendingIntent = PendingIntent.getActivity(this, 0, clickIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        notificationBuilder.apply {

            setContentIntent(clickPendingIntent)
            setContentTitle(getString(R.string.notification_content_title))
            setContentText(getString(R.string.notification_content_text, alarmDateTime.printDateShort(), alarmDateTime.printTime()))
            setSmallIcon(R.drawable.ic_alarm_orange)
            setColor(ContextCompat.getColor(this@ConfirmAlarmActivity, R.color.primary_color))
        }

        notificationManager.notify(getInt(R.integer.notification_id_upcoming_alarm), notificationBuilder.build())
    }

    private fun leaveToActiveAlarmActivity(exactAlarmTimestamp: Long){

        val activeAlarmIntent = Intent(this, ActiveAlarmActivity::class.java).apply {
            putExtra(ActiveAlarmActivity.EXTRA_ALARM_TIMESTAMP, exactAlarmTimestamp)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        startActivity(activeAlarmIntent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        mJob.cancel()
    }

    companion object {

        const val EXTRA_SLEEP_AMOUNT = "EXTRA_SLEEP_AMOUNT"
    }
}
