package com.palme.sleepwell.activity

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import com.palme.sleepwell.R
import com.palme.sleepwell.persistance.Prefs
import com.palme.sleepwell.util.*
import com.palme.sleepwell.viewmodel.ViewModelActiveAlarmActivity
import kotlinx.android.synthetic.main.activity_active_alarm.*
import org.joda.time.LocalDateTime
import java.util.*

/**
 *
 * The activity that's shown, when the user starts the app, and an alarm was activated before
 *
 * @author Leonard Palm
 */
class ActiveAlarmActivity : AppCompatActivity() {

    private val alarmManager by lazy {
        getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    private val preferences by lazy {
        Prefs(this)
    }

    private lateinit var viewModel: ViewModelActiveAlarmActivity

    private val notificationManager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_active_alarm)

        viewModel = getViewModelInstance()

        viewModel.activeAlarmTimestamp.observe(this, onAlarmTimestampChange)

        viewModel.activeAlarmTimestamp.postValue(
            intent.getLongExtra(EXTRA_ALARM_TIMESTAMP, 0L)
        )

        bnCancelAlarm.setOnClickListener { cancelCurrentAlarm() }
    }

    private val onAlarmTimestampChange = Observer<Long> {

        when (it){
            0L -> {
                leaveToSleepActivity()
            }
            else -> {
                displayAlarmStats(it)
            }
        }
    }

    private fun displayAlarmStats(alarmTimestamp: Long) {

        val alarmDateTime = LocalDateTime.fromDateFields(Date(alarmTimestamp))

        tvActiveAlarmDate.text = alarmDateTime.printDateWithWeekday()
        tvActiveAlarmTime.text = alarmDateTime.printTime()
    }

    private fun cancelCurrentAlarm(){

        viewModel.activeAlarmTimestamp.postValue(0L)

        // Re-create the exact pending intent
        val pendingAlarmIntentRecreation = PendingIntent.getBroadcast(
            this,
            viewModel.activeAlarmTimestamp.value!!.toString().hashCode(),
            Intent(this, AlarmActivity::class.java),
            PendingIntent.FLAG_CANCEL_CURRENT)

        // Cancel the alarm
        alarmManager.cancel(pendingAlarmIntentRecreation)
        pendingAlarmIntentRecreation.cancel()

        // Remove active alarm timestamp from preferences
        preferences.alarmTimestamp = 0L

        // Cancel the upcoming alarm notification
        notificationManager.cancel(getInt(R.integer.notification_id_upcoming_alarm))
    }

    private fun leaveToSleepActivity(){

        startActivitySimple(SleepSelectActivity::class.java)
        finish()
    }

    companion object {

        const val EXTRA_ALARM_TIMESTAMP = "EXTRA_ALARM_TIMESTAMP"
    }
}
