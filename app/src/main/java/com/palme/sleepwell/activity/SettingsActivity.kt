package com.palme.sleepwell.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.palme.sleepwell.R
import com.palme.sleepwell.util.getStringArray
import com.palme.sleepwell.util.getViewModelInstance
import com.palme.sleepwell.util.startActivitySimple
import com.palme.sleepwell.viewmodel.ViewModelSettingsActivity
import kotlinx.android.synthetic.main.activity_settings.*

/**
 *
 * This activity shows the settings
 *
 * @author Leonard Palm
 */
class SettingsActivity : AppCompatActivity() {

    private lateinit var viewModel: ViewModelSettingsActivity

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(com.palme.sleepwell.R.layout.activity_settings)

        viewModel = getViewModelInstance()

        setUpSpinner()

        tvSettingsImpressum.setOnClickListener(onClickImpressum)
        tvSettingLicenses.setOnClickListener(onClickLicenses)
    }

    private fun setUpSpinner(){

        // Double check if chill time is initialized
        if( viewModel.chillTime == 0 ){
            viewModel.chillTime = 5
        }

        spSettingChillTime.adapter = ArrayAdapter<String>(this,
            R.layout.spinner_item, getStringArray(R.array.spinner_selection_chill_time))
            .apply {
                setDropDownViewResource(R.layout.spinner_item)
            }

        spSettingChillTime.setSelection((viewModel.chillTime / 5) - 1)
        spSettingChillTime.onItemSelectedListener = onChillTimeSelected
    }

    private val onChillTimeSelected = object: AdapterView.OnItemSelectedListener {

        override fun onNothingSelected(parent: AdapterView<*>?) {}

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            viewModel.chillTime = ( position + 1 ) * 5
        }
    }

    private val onClickImpressum = View.OnClickListener {

        startActivitySimple(ImpressumActivity::class.java)
    }

    private val onClickLicenses = View.OnClickListener {

        startActivitySimple(LicenseReadActivity::class.java)
    }
}
