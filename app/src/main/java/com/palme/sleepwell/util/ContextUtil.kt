package com.palme.sleepwell.util

import android.content.Context
import android.content.Intent
import androidx.annotation.ArrayRes
import androidx.annotation.IntegerRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModelProviders

/**
 *
 * @author Leonard Palm
 */

/**
 * Simple shortened version to start another activity from current AppCompatActivity
 */
fun AppCompatActivity.startActivitySimple(clazz: Class<out AppCompatActivity>){
    startActivity(Intent(this, clazz))
}

/**
 * Returns the specific instance of the AndroidViewModel for the AppCompatActivity
 */
inline fun <reified T: AndroidViewModel>AppCompatActivity.getViewModelInstance(): T{
    return ViewModelProviders.of(this).get(T::class.java)
}

/**
 * Shorter version to receive an int-extra from the activities intent
 */
fun AppCompatActivity.getExtraInt(key: String): Int = this.intent.getIntExtra(key, -1)

/**
 * Shorter version to receive an int value from the activities resources
 */
fun Context.getInt(@IntegerRes resId: Int): Int
    = resources.getInteger(resId)

/**
 * Shorter version to receive an int array from the activities resources
 */
fun AppCompatActivity.getIntArray(@ArrayRes arrayResId: Int): IntArray =
    resources.getIntArray(arrayResId)

/**
 * Shorter version to receive a string array from the activities resources
 */
fun AppCompatActivity.getStringArray(@ArrayRes arrayResId: Int): Array<String> =
    resources.getStringArray(arrayResId)