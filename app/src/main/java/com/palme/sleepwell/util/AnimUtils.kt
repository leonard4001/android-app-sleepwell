package com.palme.sleepwell.util

import android.view.animation.Animation

/**
 *
 * @author Leonard Palm
 */

/**
 * Extension function, to save some lines for animation listener,
 * since in many cases only the onAnimationEnd event is needed
 */
fun Animation.addEndListener(onEnd : () -> Unit){

    setAnimationListener(object : Animation.AnimationListener{
        override fun onAnimationRepeat(animation: Animation?) { }
        override fun onAnimationEnd(animation: Animation?) { onEnd() }
        override fun onAnimationStart(animation: Animation?) { }
    })
}