package com.palme.sleepwell.util

import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

/**
 *
 * @author Leonard Palm
 */

private val locale = Locale.GERMANY
private val timeZone = TimeZone.getTimeZone("Europe/Berlin")
private val dateTimeZone = DateTimeZone.forTimeZone(timeZone)
private val formatTimeShort: DateTimeFormatter = DateTimeFormat.forPattern("HH:mm")
private val formatDateWithWeekday: DateTimeFormatter = DateTimeFormat.forPattern("EEEEEE, d.M")
private val formatDateShort: DateTimeFormatter = DateTimeFormat.forPattern("d.M")

fun currentTime(): LocalTime = LocalTime.now(dateTimeZone)

fun LocalTime.print(): String = this.toString(formatTimeShort)

fun LocalDateTime.printTime(): String = this.toString(formatTimeShort)

fun LocalDateTime.printDateWithWeekday(): String = this.toString(formatDateWithWeekday)

fun LocalDateTime.printDateShort(): String = this.toString(formatDateShort)

fun LocalDateTime.timeInMillis(): Long{
    val cal = Calendar.getInstance(timeZone, locale)
    cal.time = toDate()
    return cal.timeInMillis
}

fun getExactDateTimeForAlarm(inHours: Int, chillMinutes: Int): LocalDateTime
        = LocalDateTime.now(dateTimeZone).plusMinutes(chillMinutes).plusHours(inHours)